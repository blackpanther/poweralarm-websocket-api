﻿using System;

namespace AMWD.Net.Api.PowerAlarm.WebSocket.Connectivity
{
	internal class WebSocketReceivedEventArgs
	{
		public ArraySegment<byte> Payload { get; set; }
	}
}
