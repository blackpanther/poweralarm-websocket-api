﻿using System;
using System.Linq;
using System.Net.Http;
using System.Net.Security;
using System.Net.Sockets;
using System.Net.WebSockets;
using System.Security.Authentication;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using AMWD.Net.Api.PowerAlarm.WebSocket.Exceptions;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace AMWD.Net.Api.PowerAlarm.WebSocket.Connectivity
{
	internal class WebSocketConnection : IDisposable
	{
		private static readonly SocketError[] knownSocketErrors = new[]
		{
			SocketError.ConnectionAborted,
			SocketError.ConnectionRefused,
			SocketError.ConnectionReset,
			SocketError.HostDown,
			SocketError.HostNotFound,
			SocketError.HostUnreachable,
			SocketError.NetworkDown,
			SocketError.NetworkUnreachable,
			SocketError.NoData,
			SocketError.TimedOut,
			SocketError.TryAgain
		};

		private static readonly WebSocketState[] openStates = new[]
		{
			WebSocketState.CloseReceived,
			WebSocketState.Open
		};

		private readonly string url;
		private ClientWebSocket webSocket;

		private bool isAutoReconnect;
		private bool isAuthenticated;

		private readonly SemaphoreSlim connectLock = new(1, 1);
		private readonly SemaphoreSlim authenticateLock = new(1, 1);
		private readonly SemaphoreSlim reconnectLock = new(1, 1);

		private Task receiveTask;
		private CancellationTokenSource receiveCts;
		private CancellationTokenSource reconnectCts;

		private bool isClosing;
		private bool isReconnecting;

		public WebSocketConnection(string url)
		{
			this.url = url;
		}

		public string ApiKey { get; set; }

		public TimeSpan ConnectionTimeout { get; set; } = TimeSpan.FromSeconds(10);

#if NETSTANDARD2_1_OR_GREATER
		public RemoteCertificateValidationCallback RemoteCertificateValidationCallback { get; set; }
#endif

		public EventHandler<WebSocketReceivedEventArgs> Received { get; set; }

		public bool IsConnected => webSocket?.State == WebSocketState.Open && isAuthenticated;

		public async Task ConnectAsync(bool autoReconnect = true, CancellationToken cancellationToken = default)
		{
			MakeAssertions();

			if (IsConnected)
				throw new InvalidOperationException("Already connected");

			if (!connectLock.Wait(0))
				throw new InvalidOperationException("Already connecting");

			try
			{
				isAutoReconnect = autoReconnect;

				int retryCount = 0;
				int retryDelayIncrement = 2;
				do
				{
					try
					{
						isClosing = false;
						isAuthenticated = false;

						await StopReceiveAsync(cancellationToken);

						webSocket?.Dispose();
						webSocket = new ClientWebSocket();
#if NETSTANDARD2_1_OR_GREATER
						webSocket.Options.RemoteCertificateValidationCallback = RemoteCertificateValidationCallback;
#endif

						await Task.Delay(TimeSpan.FromSeconds(Math.Min(retryCount * retryDelayIncrement, 60)));
						retryCount++;

						using var timeoutCts = new CancellationTokenSource(ConnectionTimeout);
						using var cts = CancellationTokenSource.CreateLinkedTokenSource(timeoutCts.Token, cancellationToken);

						try
						{
							await webSocket.ConnectAsync(new Uri(url), cts.Token);
						}
						catch (OperationCanceledException) when (timeoutCts.IsCancellationRequested)
						{
							throw new SocketException((int)SocketError.TimedOut);
						}
						catch (WebSocketException ex) when (ex.InnerException is HttpRequestException && ex.InnerException.InnerException is OperationCanceledException && timeoutCts.IsCancellationRequested)
						{
							throw new SocketException((int)SocketError.TimedOut);
						}

						await AuthenticateAsync(cancellationToken);
						return;
					}
					catch (OperationCanceledException) when (cancellationToken.IsCancellationRequested)
					{
						webSocket?.Dispose();
						webSocket = null;
						return;
					}
					catch (SocketException ex) when (knownSocketErrors.Contains(ex.SocketErrorCode))
					{
						// ok
					}
					catch (WebSocketException ex) when (ex.InnerException is HttpRequestException && ex.InnerException.InnerException is SocketException socketEx && knownSocketErrors.Contains(socketEx.SocketErrorCode))
					{
						// ok
					}
					catch (WebSocketException ex) when (ex.InnerException is HttpRequestException && ex.InnerException.InnerException is AuthenticationException authEx)
					{
						// certificate validation error

						webSocket?.Dispose();
						webSocket = null;

						throw;
					}
					catch (ApiKeyException)
					{
						webSocket?.Dispose();
						webSocket = null;

						throw;
					}
					catch (Exception)
					{
						// TODO log?
						// keep it quiet for reconnect try
					}
				}
				while (isAutoReconnect && !cancellationToken.IsCancellationRequested);
			}
			finally
			{
				connectLock.Release();
			}
		}

		public Task DisconnectAsync(CancellationToken cancellationToken = default)
		{
			if (isDisposed)
				throw new ObjectDisposedException(GetType().FullName);

			isAutoReconnect = false;
			return InternalDisconnectAsync(true, cancellationToken);
		}

		public async Task AuthenticateAsync(CancellationToken cancellationToken = default)
		{
			MakeAssertions();
			if (!authenticateLock.Wait(0))
				return;

			try
			{
				if (isAuthenticated)
					return;

				string authJson = JsonConvert.SerializeObject(new { apikey = ApiKey });
				await webSocket.SendAsync(new ArraySegment<byte>(Encoding.UTF8.GetBytes(authJson)), WebSocketMessageType.Text, endOfMessage: true, cancellationToken);
				var rawResponse = await ReceiveRawMessage(cancellationToken);

				string responseJson = Encoding.UTF8.GetString(rawResponse.Array);
				responseJson = responseJson.TrimEnd('\0');
				var response = JsonConvert.DeserializeObject<JObject>(responseJson);

				if (!response["accepted"].Value<bool>())
					throw new ApiKeyException("The API key is invalid");

				isAuthenticated = true;
				Received?.Invoke(this, new WebSocketReceivedEventArgs { Payload = rawResponse });

				receiveCts = new CancellationTokenSource();
				receiveTask = ReceiveLoop(receiveCts.Token);
			}
			finally
			{
				authenticateLock.Release();
			}
		}

		public async Task StopReceiveAsync(CancellationToken cancellationToken = default)
		{
			MakeAssertions();
			if (receiveTask == null)
				return;

			if (!receiveCts.IsCancellationRequested)
				receiveCts.Cancel();

			await Task.WhenAny(receiveTask, Task.Delay(Timeout.Infinite, cancellationToken));
			receiveTask = null;

			receiveCts.Dispose();
			receiveCts = null;
		}

		private async Task<ArraySegment<byte>> ReceiveRawMessage(CancellationToken cancellationToken)
		{
			int bytesRead = 0;
			byte[] buffer = new byte[512];
			while (!cancellationToken.IsCancellationRequested)
			{
				if (webSocket == null)
					return new();

				var segment = new ArraySegment<byte>(buffer, bytesRead, buffer.Length - bytesRead);
				WebSocketReceiveResult result;
				try
				{
					result = await webSocket.ReceiveAsync(segment, cancellationToken);
				}
				catch (OperationCanceledException) when (cancellationToken.IsCancellationRequested || isClosing)
				{
					return new();
				}

				bytesRead += result.Count;

				if (result.CloseStatus.HasValue)
					return new();

				if (result.EndOfMessage)
					break;

				if (bytesRead > buffer.Length - 64)
					Array.Resize(ref buffer, buffer.Length * 2);
			}

			return new ArraySegment<byte>(buffer, 0, bytesRead);
		}

		private async Task ReceiveLoop(CancellationToken cancellationToken)
		{
			while (!cancellationToken.IsCancellationRequested)
			{
				try
				{
					var segment = await ReceiveRawMessage(cancellationToken);
					if (segment.Array?.Any() == true)
					{
						Received?.Invoke(this, new WebSocketReceivedEventArgs { Payload = segment });
					}
					else if (isClosing)
					{
						break;
					}
					else if (webSocket?.CloseStatus != null)
					{
						await InternalDisconnectAsync(false, cancellationToken);
						if (isAutoReconnect && !isReconnecting)
							Reconnect();
					}
				}
				catch (WebSocketException)
				{
					await InternalDisconnectAsync(false, cancellationToken);
					if (isAutoReconnect && !isReconnecting)
						Reconnect();
				}
				catch (OperationCanceledException) when (cancellationToken.IsCancellationRequested)
				{
					// cancellation
					continue;
				}
				catch (Exception ex)
				{
					try
					{
						await webSocket.CloseAsync(WebSocketCloseStatus.InternalServerError, ex.Message, cancellationToken);
					}
					catch
					{ /* keep it quiet */ }

					webSocket?.Dispose();
					webSocket = null;
				}
			}
		}

		private async Task InternalDisconnectAsync(bool isCloseRequest, CancellationToken cancellationToken)
		{
			try
			{
				if (webSocket == null)
					return;

				reconnectCts?.Cancel();
				receiveCts?.Cancel();

				if (isCloseRequest)
					isClosing = true;

				if (openStates.Contains(webSocket.State))
				{
					try
					{
						var closeTask = webSocket.CloseAsync(WebSocketCloseStatus.NormalClosure, "Closing connection", cancellationToken);
						await Task.WhenAny(closeTask, Task.Delay(Timeout.Infinite, cancellationToken));
					}
					catch (OperationCanceledException) when (cancellationToken.IsCancellationRequested)
					{ }
				}

				await StopReceiveAsync(cancellationToken);

				webSocket?.Dispose();
				webSocket = null;
			}
			catch (ObjectDisposedException)
			{
				// might happen when on reconnect
			}
			finally
			{
				isAuthenticated = false;
			}
		}

		private async void Reconnect()
		{
			if (!reconnectLock.Wait(0))
				return;

			isReconnecting = true;
			try
			{
				if (!isAutoReconnect)
				{
					reconnectCts?.Cancel();

					reconnectCts?.Dispose();
					reconnectCts = null;
					return;
				}

				reconnectCts = new CancellationTokenSource();
				try
				{
					await ConnectAsync(true, reconnectCts.Token);

					reconnectCts?.Dispose();
					reconnectCts = null;
					return;
				}
				catch
				{
					isAutoReconnect = false;
					reconnectCts?.Cancel();

					reconnectCts?.Dispose();
					reconnectCts = null;
					return;
				}
			}
			finally
			{
				isReconnecting = false;
				reconnectLock.Release();
			}
		}

		private bool isDisposed;

		public void Dispose()
		{
			if (isDisposed)
				return;

			isDisposed = true;

			Task.WaitAll(InternalDisconnectAsync(true, CancellationToken.None));

			connectLock.Dispose();
			authenticateLock.Dispose();
			reconnectLock.Dispose();

			webSocket?.Dispose();
		}

		private void MakeAssertions()
		{
			if (isDisposed)
				throw new ObjectDisposedException(GetType().FullName);

			if (string.IsNullOrWhiteSpace(ApiKey))
				throw new ApiKeyException("The API key is required");
		}
	}
}
