﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace AMWD.Net.Api.PowerAlarm.WebSocket.Models
{
	/// <summary>
	/// Represents details for a feedback.
	/// </summary>
	public class FeedbackDetail
	{
		/// <summary>
		/// Gets or sets the description.
		/// </summary>
		[JsonProperty("bezeichnung")]
		public string Description { get; set; }

		/// <summary>
		/// Gets or sets the color.
		/// </summary>
		[JsonProperty("farbe")]
		public string Color { get; set; }

		/// <summary>
		/// Gets or sets the list of contacts.
		/// </summary>
		[JsonProperty("kontakte")]
		public ICollection<FeedbackUser> Contacts { get; set; }
	}
}
