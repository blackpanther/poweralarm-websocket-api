﻿using System;
using Newtonsoft.Json;

namespace AMWD.Net.Api.PowerAlarm.WebSocket.Models
{
	/// <summary>
	/// Represents a vehicle.
	/// </summary>
	public class Vehicle
	{
		/// <summary>
		/// Gets or sets the id.
		/// </summary>
		[JsonProperty("id")]
		public int? Id { get; set; }

		/// <summary>
		/// Gets or sets the name.
		/// </summary>
		[JsonProperty("bezeichnung")]
		public string Name { get; set; }

		/// <summary>
		/// Gets or sets the group id.
		/// </summary>
		[JsonProperty("gruppenid")]
		public int? GroupId { get; set; }

		/// <summary>
		/// Gets or sets the FMS code.
		/// </summary>
		[JsonProperty("kennung")]
		public string Fms { get; set; }

		/// <summary>
		/// Gets or sets the OPTA code.
		/// </summary>
		[JsonProperty("opta")]
		public string Opta { get; set; }

		/// <summary>
		/// Gets or sets the status as string.
		/// </summary>
		[JsonProperty("status")]
		public string StatusStr { get; set; }

		/// <summary>
		/// Gets or sets the status.
		/// </summary>
		[JsonIgnore]
		public int? Status
		{
			get => string.IsNullOrWhiteSpace(StatusStr) ? null : int.Parse(StatusStr);
			set => StatusStr = value?.ToString();
		}

		/// <summary>
		/// Gets or sets the date (of the status).
		/// </summary>
		[JsonProperty("datum")]
		public string Date { get; set; }

		/// <summary>
		/// Gets or sets the timestamp (of the status).
		/// </summary>
		[JsonIgnore]
		public DateTime? Timestamp
		{
			get
			{
				if (DateTime.TryParse(Date, out var ts))
					return ts.Kind == DateTimeKind.Unspecified ? DateTime.SpecifyKind(ts, DateTimeKind.Local) : ts;

				return null;
			}
			set
			{
				if (!value.HasValue)
				{
					Date = null;
					return;
				}

				var ts = value.Value.Kind == DateTimeKind.Utc ? value.Value.ToLocalTime() : DateTime.SpecifyKind(value.Value, DateTimeKind.Local);
				Date = ts.ToString("yyyy-MM-dd HH:mm:ss");
			}
		}

		/// <summary>
		/// Gets or sets the color.
		/// </summary>
		[JsonProperty("color")]
		public string Color { get; set; }
	}
}
