﻿using Newtonsoft.Json;

namespace AMWD.Net.Api.PowerAlarm.WebSocket.Models
{
	/// <summary>
	/// Represents a summary of entries with a specified state.
	/// </summary>
	public class FunctionGroupCount
	{
		/// <summary>
		/// Gets or sets the status.
		/// </summary>
		[JsonProperty("status")]
		public int? Status { get; set; }

		/// <summary>
		/// Gets or sets the textual status.
		/// </summary>
		[JsonProperty("text")]
		public string Text { get; set; }

		/// <summary>
		/// Gets or sets the color.
		/// </summary>
		[JsonProperty("color")]
		public string Color { get; set; }

		/// <summary>
		/// Gets or sets the sum of entries with the state.
		/// </summary>
		[JsonProperty("count")]
		public int? Count { get; set; }
	}
}
