﻿using Newtonsoft.Json;

namespace AMWD.Net.Api.PowerAlarm.WebSocket.Models
{
	/// <summary>
	/// Represents a user on feedback.
	/// </summary>
	public class FeedbackUser
	{
		/// <summary>
		/// Gets or set the name.
		/// </summary>
		[JsonProperty("name")]
		public string Name { get; set; }

		/// <summary>
		/// Gets or sets the additional information.
		/// </summary>
		[JsonProperty("zusatz")]
		public string AdditionalInformation { get; set; }

		/// <summary>
		/// Gets or sets the replied status.
		/// </summary>
		[JsonProperty("status")]
		public string Status { get; set; }
	}
}
