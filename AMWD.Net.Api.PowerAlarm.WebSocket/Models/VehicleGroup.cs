﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace AMWD.Net.Api.PowerAlarm.WebSocket.Models
{
	/// <summary>
	/// Represents a vehicle group.
	/// </summary>
	public class VehicleGroup
	{
		/// <summary>
		/// Gets or sets the id (when <c>null</c> try <see cref="GroupId"/>).
		/// </summary>
		[JsonProperty("id")]
		public int? Id { get; set; }

		/// <summary>
		/// Gets or sets the group's id (when <c>null</c> try <see cref="Id"/>).
		/// </summary>
		[JsonProperty("gruppenid")]
		public int? GroupId { get; set; }

		/// <summary>
		/// Gets or sets the name.
		/// </summary>
		[JsonProperty("bezeichnung")]
		public string Name { get; set; }

		/// <summary>
		/// Gets or sets a list of vehicles.
		/// </summary>
		[JsonProperty("details")]
		public ICollection<Vehicle> Vehicles { get; set; }

		/// <summary>
		/// Returns the id or zero (0).
		/// </summary>
		/// <returns></returns>
		public int GetId() => Id ?? GroupId ?? 0;
	}
}
