﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace AMWD.Net.Api.PowerAlarm.WebSocket.Models
{
	/// <summary>
	/// Represents the user feedback for an alarm.
	/// </summary>
	public class Feedback
	{
		/// <summary>
		/// Gets or sets the id as string.
		/// </summary>
		[JsonProperty("id")]
		public string IdStr { get; set; }

		/// <summary>
		/// Gets or sets the id.
		/// </summary>
		[JsonIgnore]
		public int? Id
		{
			get => int.TryParse(IdStr, out int id) ? id : null;
			set => IdStr = value?.ToString();
		}

		/// <summary>
		/// Gets or sets the message.
		/// </summary>
		[JsonProperty("text")]
		public string Message { get; set; }

		/// <summary>
		/// Gets or sets the date.
		/// </summary>
		[JsonProperty("datum")]
		public string Date { get; set; }

		/// <summary>
		/// Gets or sets the timestamp.
		/// </summary>
		[JsonIgnore]
		public DateTime? Timestamp
		{
			get
			{
				if (DateTime.TryParse(Date, out var ts))
					return ts.Kind == DateTimeKind.Unspecified ? DateTime.SpecifyKind(ts, DateTimeKind.Local) : ts;

				return null;
			}
			set
			{
				if (!value.HasValue)
				{
					Date = null;
					return;
				}

				var ts = value.Value.Kind == DateTimeKind.Utc ? value.Value.ToLocalTime() : DateTime.SpecifyKind(value.Value, DateTimeKind.Local);
				Date = ts.ToString("yyyy-MM-dd HH:mm:ss");
			}
		}

		/// <summary>
		/// Gets or sets details.
		/// </summary>
		public ICollection<FeedbackDetail> Details { get; set; }
	}
}
