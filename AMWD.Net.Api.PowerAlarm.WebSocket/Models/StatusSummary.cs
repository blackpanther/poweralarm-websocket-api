﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace AMWD.Net.Api.PowerAlarm.WebSocket.Models
{
	/// <summary>
	/// Represents a status summary.
	/// </summary>
	public class StatusSummary
	{
		/// <summary>
		/// Gets or sets the date.
		/// </summary>
		[JsonProperty("datum")]
		public string Date { get; set; }

		/// <summary>
		/// Gets or sets a timestamp.
		/// </summary>
		[JsonIgnore]
		public DateTime? Timestamp
		{
			get
			{
				if (DateTime.TryParse(Date, out var ts))
					return ts.Kind == DateTimeKind.Unspecified ? DateTime.SpecifyKind(ts, DateTimeKind.Local) : ts;

				return null;
			}
			set
			{
				if (!value.HasValue)
				{
					Date = null;
					return;
				}

				var ts = value.Value.Kind == DateTimeKind.Utc ? value.Value.ToLocalTime() : DateTime.SpecifyKind(value.Value, DateTimeKind.Local);
				Date = ts.ToString("yyyy-MM-dd HH:mm:ss");
			}
		}

		/// <summary>
		/// Gets or sets a list of function groups.
		/// </summary>
		[JsonProperty("fgruppe")]
		public ICollection<FunctionGroup> FunctionGroups { get; set; }
	}
}
