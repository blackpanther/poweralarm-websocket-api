﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace AMWD.Net.Api.PowerAlarm.WebSocket.Models
{
	/// <summary>
	/// Represents a function group.
	/// </summary>
	public class FunctionGroup
	{
		/// <summary>
		/// Gets or sets the id.
		/// </summary>
		[JsonProperty("id")]
		public int? Id { get; set; }

		/// <summary>
		/// Gets or sets a text.
		/// </summary>
		[JsonProperty("text")]
		public string Message { get; set; }

		/// <summary>
		/// Gets or sets a list of status sums.
		/// </summary>
		[JsonProperty("data")]
		public ICollection<FunctionGroupCount> GroupCounts { get; set; }
	}
}
