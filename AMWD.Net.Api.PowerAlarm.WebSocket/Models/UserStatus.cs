﻿using System.Globalization;
using Newtonsoft.Json;

namespace AMWD.Net.Api.PowerAlarm.WebSocket.Models
{
	/// <summary>
	/// Represents a user status.
	/// </summary>
	public class UserStatus
	{
		/// <summary>
		/// Gets or sets the status as string.
		/// </summary>
		[JsonProperty("status")]
		public string StatusStr { get; set; }

		/// <summary>
		/// Gets or sets the status.
		/// </summary>
		[JsonIgnore]
		public int? Status
		{
			get => int.TryParse(StatusStr, out int status) ? status : null;
			set => StatusStr = value?.ToString();
		}

		/// <summary>
		/// Gets or sets the name.
		/// </summary>
		[JsonProperty("name")]
		public string Name { get; set; }

		/// <summary>
		/// Gets or sets additional information.
		/// </summary>
		[JsonProperty("zusatz")]
		public string AdditionalInformation { get; set; }

		/// <summary>
		/// Gets or sets the latitude of the users position.
		/// </summary>
		[JsonProperty("lat")]
		public string LatStr { get; set; }

		/// <summary>
		/// Gets or sets the latitude of the users position.
		/// </summary>
		[JsonIgnore]
		public float? Latitude
		{
			get => float.TryParse(LatStr, NumberStyles.Float, CultureInfo.InvariantCulture, out float lat) ? lat : null;
			set => LatStr = value?.ToString(CultureInfo.InvariantCulture);
		}

		/// <summary>
		/// Gets or sets the longitude of the users position.
		/// </summary>
		[JsonProperty("long")]
		public string LngStr { get; set; }

		/// <summary>
		/// Gets or sets the longitude of the users position.
		/// </summary>
		[JsonIgnore]
		public float? Longitude
		{
			get => float.TryParse(LngStr, NumberStyles.Float, CultureInfo.InvariantCulture, out float lng) ? lng : null;
			set => LngStr = value?.ToString(CultureInfo.InvariantCulture);
		}
	}
}
