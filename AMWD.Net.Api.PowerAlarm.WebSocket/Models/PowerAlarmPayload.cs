﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace AMWD.Net.Api.PowerAlarm.WebSocket.Models
{
	internal class PowerAlarmPayload
	{
		[JsonProperty("date")]
		public string Date { get; set; }

		[JsonIgnore]
		public DateTime? Timestamp
		{
			get
			{
				if (DateTime.TryParse(Date, out var ts))
					return ts.Kind == DateTimeKind.Unspecified ? DateTime.SpecifyKind(ts, DateTimeKind.Utc) : ts;

				return null;
			}
			set
			{
				if (!value.HasValue)
				{
					Date = null;
					return;
				}

				var ts = value.Value.Kind == DateTimeKind.Local ? value.Value.ToUniversalTime() : DateTime.SpecifyKind(value.Value, DateTimeKind.Utc);
				Date = ts.ToString("yyyy-MM-dd'T'HH:mm:ss.fffK");
			}
		}

		[JsonProperty("accepted")]
		public bool? IsAccepted { get; set; }

		[JsonProperty("ws")]
		public bool? IsWebSocket { get; set; }

		[JsonProperty("status_summary")]
		public StatusSummary StatusSummary { get; set; }

		[JsonProperty("status")]
		public UserStatus Status { get; set; }

		[JsonProperty("news_summary")]
		public ICollection<News> NewsSummary { get; set; }

		[JsonProperty("fahrzeug_summary")]
		public ICollection<VehicleGroup> VehicleSummary { get; set; }

		[JsonProperty("feedback_summary")]
		public ICollection<FeedbackSummary> FeedbackSummary { get; set; }

		[JsonProperty("feedback")]
		public Feedback Feedback { get; set; }
	}
}
