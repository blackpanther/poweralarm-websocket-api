﻿using System;
using Newtonsoft.Json;

namespace AMWD.Net.Api.PowerAlarm.WebSocket.Models
{
	/// <summary>
	/// Represents a news message.
	/// </summary>
	public class News
	{
		/// <summary>
		/// Gets or sets the id.
		/// </summary>
		[JsonProperty("id")]
		public int? Id { get; set; }

		/// <summary>
		/// Gets or sets the message.
		/// </summary>
		[JsonProperty("text")]
		public string Message { get; set; }

		/// <summary>
		/// Gets or sets the start date.
		/// </summary>
		[JsonProperty("startdate")]
		public string StartDate { get; set; }

		/// <summary>
		/// Gets or sets the start timestmap;
		/// </summary>
		[JsonIgnore]
		public DateTime? StartTimestamp
		{
			get
			{
				if (DateTime.TryParse(StartDate, out var ts))
					return ts.Kind == DateTimeKind.Unspecified ? DateTime.SpecifyKind(ts, DateTimeKind.Local) : ts;

				return null;
			}
			set
			{
				if (!value.HasValue)
				{
					StartDate = null;
					return;
				}

				var ts = value.Value.Kind == DateTimeKind.Utc ? value.Value.ToLocalTime() : DateTime.SpecifyKind(value.Value, DateTimeKind.Local);
				StartDate = ts.ToString("yyyy-MM-dd HH:mm:ss");
			}
		}

		/// <summary>
		/// Gets or sets the end date.
		/// </summary>
		[JsonProperty("enddate")]
		public string EndDate { get; set; }

		/// <summary>
		/// Gets or sets the end timestmap;
		/// </summary>
		[JsonIgnore]
		public DateTime? EndTimestamp
		{
			get
			{
				if (DateTime.TryParse(EndDate, out var ts))
					return ts.Kind == DateTimeKind.Unspecified ? DateTime.SpecifyKind(ts, DateTimeKind.Local) : ts;

				return null;
			}
			set
			{
				if (!value.HasValue)
				{
					EndDate = null;
					return;
				}

				var ts = value.Value.Kind == DateTimeKind.Utc ? value.Value.ToLocalTime() : DateTime.SpecifyKind(value.Value, DateTimeKind.Local);
				EndDate = ts.ToString("yyyy-MM-dd HH:mm:ss");
			}
		}
	}
}
