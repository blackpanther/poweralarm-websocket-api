﻿using System;
using AMWD.Net.Api.PowerAlarm.WebSocket.Enums;

namespace AMWD.Net.Api.PowerAlarm.WebSocket
{
	/// <summary>
	/// Represents the event for new PowerAlarm data.
	/// </summary>
	/// <remarks>
	/// The <see cref="Type"/> will give you the hint in which generic type you'll need to specialize it.
	/// </remarks>
	public class PowerAlarmEventArgs : EventArgs
	{
		/// <summary>
		/// Gets or sets the timestamp of the event.
		/// </summary>
		public DateTime? Timestamp { get; set; }

		/// <summary>
		/// Gets or sets the type of the event.
		/// </summary>
		public PowerAlarmEventType Type { get; set; }

		/// <summary>
		/// Returns a specialized event.
		/// </summary>
		/// <typeparam name="T">The resulting event type.</typeparam>
		/// <returns></returns>
		public PowerAlarmEventArgs<T> GetSpecialized<T>() => (PowerAlarmEventArgs<T>)this;

		/// <summary>
		/// Returns the payload of the specialized event.
		/// </summary>
		/// <typeparam name="T">The resulting event type.</typeparam>
		/// <returns></returns>
		public T GetPayload<T>()
			=> GetSpecialized<T>().Payload;
	}

	/// <summary>
	/// Represents the specialized event for new PowerAlarm data.
	/// </summary>
	/// <typeparam name="T">The resulting event type.</typeparam>
	public class PowerAlarmEventArgs<T> : PowerAlarmEventArgs
	{
		/// <summary>
		/// Gets or sets the specialized payload.
		/// </summary>
		public T Payload { get; set; }
	}
}
