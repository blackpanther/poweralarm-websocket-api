﻿using System;
using System.Text;
using Newtonsoft.Json;

namespace AMWD.Net.Api.PowerAlarm.WebSocket.Utils
{
	internal static class ArraySegmentExtensions
	{
		private static readonly JsonSerializerSettings serializerSettings = new()
		{
			NullValueHandling = NullValueHandling.Ignore,
			Formatting = Formatting.None
		};

		public static ArraySegment<byte> SerializeArraySegment<T>(this T obj)
		{
			string json = JsonConvert.SerializeObject(obj, serializerSettings);
			byte[] bytes = Encoding.UTF8.GetBytes(json);
			return new ArraySegment<byte>(bytes);
		}

		public static T DeserializeArraySegment<T>(this ArraySegment<byte> segment)
		{
			string json = Encoding.UTF8.GetString(segment.Array);
			return JsonConvert.DeserializeObject<T>(json, serializerSettings);
		}
	}
}
