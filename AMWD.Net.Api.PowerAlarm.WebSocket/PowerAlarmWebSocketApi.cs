﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Security;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using AMWD.Net.Api.PowerAlarm.WebSocket.Connectivity;
using AMWD.Net.Api.PowerAlarm.WebSocket.Enums;
using AMWD.Net.Api.PowerAlarm.WebSocket.EventArguments;
using AMWD.Net.Api.PowerAlarm.WebSocket.Exceptions;
using AMWD.Net.Api.PowerAlarm.WebSocket.Models;
using Microsoft.Extensions.Hosting;
using Newtonsoft.Json;

[assembly: System.Runtime.CompilerServices.InternalsVisibleTo("AMWD.Net.Api.PowerAlarm.WebSocket.Test")]

namespace AMWD.Net.Api.PowerAlarm.WebSocket
{
	/// <summary>
	/// Implements the WebSocket interface of PowerAlarm (poweralarm.de).
	/// </summary>
	public class PowerAlarmWebSocketApi : IHostedService, IDisposable
	{
		/// <summary>
		/// The "normal" websocket address without TLS encryption.
		/// </summary>
		public const string SOCKET_URL = "ws://www.poweralarm.de:8000";

		/// <summary>
		/// The secured websocket address with TLS encryption.
		/// </summary>
		public const string SOCKET_URL_SECURE = "wss://www.poweralarm.de:8001";

		private readonly WebSocketConnection webSocket;

		/// <summary>
		/// Initializes a new instance of the <see cref="PowerAlarmWebSocketApi"/> class.
		/// </summary>
		public PowerAlarmWebSocketApi()
			: this(false)
		{ }

		/// <summary>
		/// Initializes a new instance of the <see cref="PowerAlarmWebSocketApi"/> class.
		/// </summary>
		/// <param name="useInsecureConnection">A value indicating whether to use the "normal" address.</param>
		public PowerAlarmWebSocketApi(bool useInsecureConnection)
			: this(useInsecureConnection ? SOCKET_URL : SOCKET_URL_SECURE)
		{ }

		/// <summary>
		/// Initializes a new instance of the <see cref="PowerAlarmWebSocketApi"/> class.
		/// </summary>
		/// <param name="url">A websockt url to connect to.</param>
		internal PowerAlarmWebSocketApi(string url)
		{
			webSocket = new WebSocketConnection(url);
			webSocket.Received += OnReceived;
		}

		/// <summary>
		/// Gets or sets the API key.
		/// </summary>
		public string ApiKey
		{
			get => webSocket.ApiKey;
			set => webSocket.ApiKey = value;
		}

#if NETSTANDARD2_1_OR_GREATER

		/// <summary>
		/// Gets or set a callback to validate the remote certificate.
		/// </summary>
		public RemoteCertificateValidationCallback RemoteCertificateValidationCallback
		{
			get => webSocket.RemoteCertificateValidationCallback;
			set => webSocket.RemoteCertificateValidationCallback = value;
		}

#endif

		#region Events

		/// <summary>
		/// Raised when a new information is received.
		/// </summary>
		/// <remarks>
		/// The <see cref="PowerAlarmEventArgs.Type"/> will specify which payload is provided:
		/// <list type="bullet">
		/// <item><see cref="PowerAlarmEventType.StatusSummary"/> => <see cref="StatusSummary"/></item>
		/// <item><see cref="PowerAlarmEventType.Status"/> => <see cref="UserStatus"/></item>
		/// <item><see cref="PowerAlarmEventType.NewsSummary"/> => <see cref="List{T}"/> where <c>T</c> is <see cref="News"/></item>
		/// <item><see cref="PowerAlarmEventType.VehicleSummary"/> => <see cref="List{T}"/> where <c>T</c> is <see cref="VehicleGroup"/></item>
		/// <item><see cref="PowerAlarmEventType.FeedbackSummary"/> => <see cref="List{T}"/> where <c>T</c> is <see cref="FeedbackSummary"/></item>
		/// <item><see cref="PowerAlarmEventType.Feedback"/> => <see cref="Feedback"/></item>
		/// </list>
		/// </remarks>
		public event EventHandler<PowerAlarmEventArgs> Received;

		/// <summary>
		/// Raised when a new status summary is received.
		/// </summary>
		public event EventHandler<StatusSummaryEventArgs> ReceivedStatusSummary;

		/// <summary>
		/// Raised when a new status (of a user) is received.
		/// </summary>
		public event EventHandler<StatusEventArgs> ReceivedStatus;

		/// <summary>
		/// Raised when a new news summary is received.
		/// </summary>
		public event EventHandler<NewsSummaryEventArgs> ReceivedNewsSummary;

		/// <summary>
		/// Raised when a new vehicle summary is received.
		/// </summary>
		public event EventHandler<VehicleSummaryEventArgs> ReceivedVehicleSummary;

		/// <summary>
		/// Raised when a new feedback summary is received.
		/// </summary>
		public event EventHandler<FeedbackSummaryEventArgs> ReceivedFeedbackSummary;

		/// <summary>
		/// Raised when a new feedback is received.
		/// </summary>
		public event EventHandler<FeedbackEventArgs> ReceivedFeedback;

		#endregion Events

		#region IHostedService implementation

		/// <inheritdoc/>
		public Task StartAsync(CancellationToken cancellationToken)
		{
			MakeAsserts();
			return webSocket.ConnectAsync(true, cancellationToken);
		}

		/// <inheritdoc/>
		public Task StopAsync(CancellationToken cancellationToken)
		{
			MakeAsserts();
			return InternalStopAsync(cancellationToken);
		}

		#endregion IHostedService implementation

		private Task InternalStopAsync(CancellationToken cancellationToken)
		{
			return webSocket.DisconnectAsync(cancellationToken);
		}

		private void OnReceived(object sender, WebSocketReceivedEventArgs eventArguments)
		{
			string rawPayload = Encoding.UTF8.GetString(eventArguments.Payload.Array).TrimEnd('\0');
			try
			{
				var payload = JsonConvert.DeserializeObject<PowerAlarmPayload>(rawPayload);
				bool isKnown = false;

				if (payload.StatusSummary != null)
				{
					Received?.Invoke(this, new PowerAlarmEventArgs<StatusSummary>
					{
						Timestamp = payload.Timestamp,
						Type = PowerAlarmEventType.StatusSummary,
						Payload = payload.StatusSummary
					});
					ReceivedStatusSummary?.Invoke(this, new StatusSummaryEventArgs
					{
						Timestamp = payload.Timestamp,
						Summary = payload.StatusSummary
					});
					isKnown = true;
				}

				if (payload.Status != null)
				{
					Received?.Invoke(this, new PowerAlarmEventArgs<UserStatus>
					{
						Timestamp = payload.Timestamp,
						Type = PowerAlarmEventType.Status,
						Payload = payload.Status
					});
					ReceivedStatus?.Invoke(this, new StatusEventArgs
					{
						Timestamp = payload.Timestamp,
						Status = payload.Status
					});
					isKnown = true;
				}

				if (payload.NewsSummary?.Any() == true)
				{
					Received?.Invoke(this, new PowerAlarmEventArgs<List<News>>
					{
						Timestamp = payload.Timestamp,
						Type = PowerAlarmEventType.NewsSummary,
						Payload = payload.NewsSummary.ToList()
					});
					ReceivedNewsSummary?.Invoke(this, new NewsSummaryEventArgs
					{
						Timestamp = payload.Timestamp,
						NewsList = payload.NewsSummary.ToList()
					});
					isKnown = true;
				}

				if (payload.VehicleSummary?.Any() == true)
				{
					Received?.Invoke(this, new PowerAlarmEventArgs<List<VehicleGroup>>
					{
						Timestamp = payload.Timestamp,
						Type = PowerAlarmEventType.VehicleSummary,
						Payload = payload.VehicleSummary.ToList()
					});
					ReceivedVehicleSummary?.Invoke(this, new VehicleSummaryEventArgs
					{
						Timestamp = payload.Timestamp,
						VehicleGroups = payload.VehicleSummary.ToList()
					});
					isKnown = true;
				}

				if (payload.FeedbackSummary?.Any() == true)
				{
					Received?.Invoke(this, new PowerAlarmEventArgs<List<FeedbackSummary>>
					{
						Timestamp = payload.Timestamp,
						Type = PowerAlarmEventType.FeedbackSummary,
						Payload = payload.FeedbackSummary.ToList()
					});
					ReceivedFeedbackSummary?.Invoke(this, new FeedbackSummaryEventArgs
					{
						Timestamp = payload.Timestamp,
						FeedbackList = payload.FeedbackSummary.ToList()
					});
					isKnown = true;
				}

				if (payload.Feedback != null)
				{
					Received?.Invoke(this, new PowerAlarmEventArgs<Feedback>
					{
						Timestamp = payload.Timestamp,
						Type = PowerAlarmEventType.Feedback,
						Payload = payload.Feedback
					});
					ReceivedFeedback?.Invoke(this, new FeedbackEventArgs
					{
						Timestamp = payload.Timestamp,
						Feedback = payload.Feedback
					});
					isKnown = true;
				}

				if (!isKnown)
				{
					Received?.Invoke(this, new PowerAlarmEventArgs<string>
					{
						Timestamp = payload.Timestamp,
						Type = PowerAlarmEventType.Undefined,
						Payload = rawPayload
					});
				}
			}
			catch
			{
				Received?.Invoke(this, new PowerAlarmEventArgs<string>
				{
					Timestamp = DateTime.Now,
					Type = PowerAlarmEventType.Undefined,
					Payload = rawPayload
				});
			}
		}

		#region IDisposable implementation

		private bool isDisposed;

		/// <inheritdoc/>
		public void Dispose()
		{
			if (isDisposed)
				return;

			isDisposed = true;

			Task.WaitAll(InternalStopAsync(CancellationToken.None));

			webSocket.Received -= OnReceived;
			webSocket.Dispose();
		}

		private void MakeAsserts()
		{
			if (isDisposed)
				throw new ObjectDisposedException(GetType().FullName);

			if (string.IsNullOrWhiteSpace(ApiKey))
				throw new ApiKeyException("The API key is required");
		}

		#endregion IDisposable implementation
	}
}
