﻿namespace AMWD.Net.Api.PowerAlarm.WebSocket.Enums
{
	/// <summary>
	/// Lists the known event types.
	/// </summary>
	public enum PowerAlarmEventType
	{
		/// <summary>
		/// Undefined event type => see JSON serialized string.
		/// </summary>
		Undefined,

		/// <summary>
		/// A status summary.
		/// </summary>
		StatusSummary,

		/// <summary>
		/// A (user) status.
		/// </summary>
		Status,

		/// <summary>
		/// A news summary.
		/// </summary>
		NewsSummary,

		/// <summary>
		/// A vehicle summary.
		/// </summary>
		VehicleSummary,

		/// <summary>
		/// A feedback summary.
		/// </summary>
		FeedbackSummary,

		/// <summary>
		/// A feedback.
		/// </summary>
		Feedback
	}
}
