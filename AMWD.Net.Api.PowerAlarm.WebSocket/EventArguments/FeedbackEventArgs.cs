﻿using System;
using AMWD.Net.Api.PowerAlarm.WebSocket.Models;

namespace AMWD.Net.Api.PowerAlarm.WebSocket.EventArguments
{
	/// <summary>
	/// Represents the event for a new feedback.
	/// </summary>
	public class FeedbackEventArgs : EventArgs
	{
		/// <summary>
		/// Gets or sets the timestamp.
		/// </summary>
		public DateTime? Timestamp { get; set; }

		/// <summary>
		/// Gets or sets the feedback.
		/// </summary>
		public Feedback Feedback { get; set; }
	}
}
