﻿using System;
using System.Collections.Generic;
using AMWD.Net.Api.PowerAlarm.WebSocket.Models;

namespace AMWD.Net.Api.PowerAlarm.WebSocket.EventArguments
{
	/// <summary>
	/// Represents the event for a new feedback summary.
	/// </summary>
	public class FeedbackSummaryEventArgs : EventArgs
	{
		/// <summary>
		/// Gets or sets the timestamp.
		/// </summary>
		public DateTime? Timestamp { get; set; }

		/// <summary>
		/// Gets or sets a list of feedback summaries.
		/// </summary>
		public List<FeedbackSummary> FeedbackList { get; set; }
	}
}
