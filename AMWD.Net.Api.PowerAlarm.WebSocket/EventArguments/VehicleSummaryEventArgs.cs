﻿using System;
using System.Collections.Generic;
using AMWD.Net.Api.PowerAlarm.WebSocket.Models;

namespace AMWD.Net.Api.PowerAlarm.WebSocket.EventArguments
{
	/// <summary>
	/// Represents the event for a new vehicle summary.
	/// </summary>
	public class VehicleSummaryEventArgs : EventArgs
	{
		/// <summary>
		/// Gets or sets the timestamp.
		/// </summary>
		public DateTime? Timestamp { get; set; }

		/// <summary>
		/// Gets or sets a list of vehicle groups.
		/// </summary>
		public List<VehicleGroup> VehicleGroups { get; set; }
	}
}
