﻿using System;
using AMWD.Net.Api.PowerAlarm.WebSocket.Models;

namespace AMWD.Net.Api.PowerAlarm.WebSocket.EventArguments
{
	/// <summary>
	/// Represents the event for a new status summary.
	/// </summary>
	public class StatusSummaryEventArgs : EventArgs
	{
		/// <summary>
		/// Gets or sets the timestamp.
		/// </summary>
		public DateTime? Timestamp { get; set; }

		/// <summary>
		/// Gets or sets the status summary.
		/// </summary>
		public StatusSummary Summary { get; set; }
	}
}
