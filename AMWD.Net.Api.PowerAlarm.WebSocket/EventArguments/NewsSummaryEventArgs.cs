﻿using System;
using System.Collections.Generic;
using AMWD.Net.Api.PowerAlarm.WebSocket.Models;

namespace AMWD.Net.Api.PowerAlarm.WebSocket.EventArguments
{
	/// <summary>
	/// Represents the event for a new news summary.
	/// </summary>
	public class NewsSummaryEventArgs : EventArgs
	{
		/// <summary>
		/// Gets or sets the timestamp.
		/// </summary>
		public DateTime? Timestamp { get; set; }

		/// <summary>
		/// Gets or sets a list of news.
		/// </summary>
		public List<News> NewsList { get; set; }
	}
}
