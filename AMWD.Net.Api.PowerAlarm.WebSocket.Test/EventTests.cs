﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AMWD.Net.Api.PowerAlarm.WebSocket.Enums;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;

namespace AMWD.Net.Api.PowerAlarm.WebSocket.Test
{
	[TestClass]
	[System.Diagnostics.CodeAnalysis.ExcludeFromCodeCoverage]
	public class EventTests : TestBase
	{
		private int eventRaiseTime = 1000;

		[TestMethod]
		public async Task ShouldReceiveEventEvenForBrokenMessages()
		{
			// arrange
			string badText = "You shall not pass the process";
			string apiKey = "abc";
			responseQueue.Enqueue(JsonConvert.SerializeObject(authorized));

			using var server = CreateServer();
			using var api = GetApi(apiKey);

			// act
			await api.StartAsync(CancellationToken.None);
			await Task.Delay(eventRaiseTime); // ensure event to be raised

			await server.SendMessage(badText);
			await Task.Delay(eventRaiseTime); // ensure event to be raised

			await api.StopAsync(CancellationToken.None);

			// assert
			Assert.AreEqual(1, requestCallback.Count);
			Assert.AreEqual($"{{\"apikey\":\"{apiKey}\"}}", requestCallback.First());

			Assert.AreEqual(2, receivedEvents.Count);
			Assert.AreEqual(PowerAlarmEventType.Undefined, receivedEvents[1].Type);
			// Timestamp not to validate due to "current"
			Assert.AreEqual(badText, receivedEvents[1].GetPayload<string>());
		}

		[TestMethod]
		public async Task ShouldReceiveFeedback()
		{
			// arrange
			string apiKey = "abc";
			responseQueue.Enqueue(JsonConvert.SerializeObject(authorized));

			//payload.Feedback = null;
			payload.FeedbackSummary = null;
			payload.NewsSummary = null;
			payload.Status = null;
			payload.StatusSummary = null;
			payload.VehicleSummary = null;

			using var server = CreateServer();
			using var api = GetApi(apiKey);

			// act
			await api.StartAsync(CancellationToken.None);
			await Task.Delay(eventRaiseTime); // ensure event to be raised

			await server.SendMessage(JsonConvert.SerializeObject(payload));
			await Task.Delay(eventRaiseTime); // ensure event to be raised

			await api.StopAsync(CancellationToken.None);

			// assert
			Assert.AreEqual(1, requestCallback.Count);
			Assert.AreEqual($"{{\"apikey\":\"{apiKey}\"}}", requestCallback.First());

			Assert.AreEqual(2, receivedEvents.Count);
			Assert.AreEqual(1, receivedFeedback.Count);
			Assert.AreEqual(0, receivedFeedbackSummary.Count);
			Assert.AreEqual(0, receivedNewsSummary.Count);
			Assert.AreEqual(0, receivedStatus.Count);
			Assert.AreEqual(0, receivedStatusSummary.Count);
			Assert.AreEqual(0, receivedVehicleSummary.Count);

			Assert.AreEqual(date, receivedFeedback.First().Timestamp?.ToUniversalTime());
			Assert.IsNotNull(receivedFeedback.First().Feedback);

			Assert.AreEqual(date, receivedFeedback.First().Feedback.Timestamp?.ToUniversalTime());
			Assert.AreEqual(payload.Feedback.Id, receivedFeedback.First().Feedback.Id);
			Assert.AreEqual(payload.Feedback.Message, receivedFeedback.First().Feedback.Message);

			Assert.AreEqual(JsonConvert.SerializeObject(payload.Feedback.Details), JsonConvert.SerializeObject(receivedFeedback.First().Feedback.Details));
		}

		[TestMethod]
		public async Task ShouldReceiveFeedbackSummary()
		{
			// arrange
			string apiKey = "abc";
			responseQueue.Enqueue(JsonConvert.SerializeObject(authorized));

			payload.Feedback = null;
			//payload.FeedbackSummary = null;
			payload.NewsSummary = null;
			payload.Status = null;
			payload.StatusSummary = null;
			payload.VehicleSummary = null;

			using var server = CreateServer();
			using var api = GetApi(apiKey);

			// act
			await api.StartAsync(CancellationToken.None);
			await Task.Delay(eventRaiseTime); // ensure event to be raised

			await server.SendMessage(JsonConvert.SerializeObject(payload));
			await Task.Delay(eventRaiseTime); // ensure event to be raised

			await api.StopAsync(CancellationToken.None);

			// assert
			Assert.AreEqual(1, requestCallback.Count);
			Assert.AreEqual($"{{\"apikey\":\"{apiKey}\"}}", requestCallback.First());

			Assert.AreEqual(2, receivedEvents.Count);
			Assert.AreEqual(0, receivedFeedback.Count);
			Assert.AreEqual(1, receivedFeedbackSummary.Count);
			Assert.AreEqual(0, receivedNewsSummary.Count);
			Assert.AreEqual(0, receivedStatus.Count);
			Assert.AreEqual(0, receivedStatusSummary.Count);
			Assert.AreEqual(0, receivedVehicleSummary.Count);

			Assert.AreEqual(date, receivedFeedbackSummary.First().Timestamp?.ToUniversalTime());
			Assert.IsNotNull(receivedFeedbackSummary.First().FeedbackList);

			for (int i = 0; i < receivedFeedbackSummary.First().FeedbackList.Count; i++)
			{
				Assert.AreEqual(payload.FeedbackSummary.ElementAt(i).Id, receivedFeedbackSummary.First().FeedbackList.ElementAt(i).Id);
				Assert.AreEqual(date, receivedFeedbackSummary.First().FeedbackList.ElementAt(i).Timestamp.Value.ToUniversalTime());
				Assert.AreEqual(payload.FeedbackSummary.ElementAt(i).Group, receivedFeedbackSummary.First().FeedbackList.ElementAt(i).Group);
				Assert.AreEqual(payload.FeedbackSummary.ElementAt(i).Message, receivedFeedbackSummary.First().FeedbackList.ElementAt(i).Message);

				Assert.AreEqual(JsonConvert.SerializeObject(payload.FeedbackSummary.ElementAt(i).FunctionGroups), JsonConvert.SerializeObject(receivedFeedbackSummary.First().FeedbackList.ElementAt(i).FunctionGroups));
			}
		}

		[TestMethod]
		public async Task ShouldReceiveNewsSummary()
		{
			// arrange
			string apiKey = "abc";
			responseQueue.Enqueue(JsonConvert.SerializeObject(authorized));

			payload.Feedback = null;
			payload.FeedbackSummary = null;
			//payload.NewsSummary = null;
			payload.Status = null;
			payload.StatusSummary = null;
			payload.VehicleSummary = null;

			using var server = CreateServer();
			using var api = GetApi(apiKey);

			// act
			await api.StartAsync(CancellationToken.None);
			await Task.Delay(eventRaiseTime); // ensure event to be raised

			await server.SendMessage(JsonConvert.SerializeObject(payload));
			await Task.Delay(eventRaiseTime); // ensure event to be raised

			await api.StopAsync(CancellationToken.None);

			// assert
			Assert.AreEqual(1, requestCallback.Count);
			Assert.AreEqual($"{{\"apikey\":\"{apiKey}\"}}", requestCallback.First());

			Assert.AreEqual(2, receivedEvents.Count);
			Assert.AreEqual(0, receivedFeedback.Count);
			Assert.AreEqual(0, receivedFeedbackSummary.Count);
			Assert.AreEqual(1, receivedNewsSummary.Count);
			Assert.AreEqual(0, receivedStatus.Count);
			Assert.AreEqual(0, receivedStatusSummary.Count);
			Assert.AreEqual(0, receivedVehicleSummary.Count);

			Assert.AreEqual(date, receivedNewsSummary.First().Timestamp?.ToUniversalTime());
			Assert.IsNotNull(receivedNewsSummary.First().NewsList);

			for (int i = 0; i < receivedNewsSummary.First().NewsList.Count; i++)
			{
				Assert.AreEqual(payload.NewsSummary.ElementAt(i).Id, receivedNewsSummary.First().NewsList.ElementAt(i).Id);
				Assert.AreEqual(date, receivedNewsSummary.First().NewsList.ElementAt(i).StartTimestamp.Value.ToUniversalTime());
				Assert.AreEqual(date.AddHours(1), receivedNewsSummary.First().NewsList.ElementAt(i).EndTimestamp.Value.ToUniversalTime());
				Assert.AreEqual(payload.NewsSummary.ElementAt(i).Message, receivedNewsSummary.First().NewsList.ElementAt(i).Message);
			}
		}

		[TestMethod]
		public async Task ShouldReceiveStatus()
		{
			// arrange
			string apiKey = "abc";
			responseQueue.Enqueue(JsonConvert.SerializeObject(authorized));

			payload.Feedback = null;
			payload.FeedbackSummary = null;
			payload.NewsSummary = null;
			//payload.Status = null;
			payload.StatusSummary = null;
			payload.VehicleSummary = null;

			using var server = CreateServer();
			using var api = GetApi(apiKey);

			// act
			await api.StartAsync(CancellationToken.None);
			await Task.Delay(eventRaiseTime); // ensure event to be raised

			await server.SendMessage(JsonConvert.SerializeObject(payload));
			await Task.Delay(eventRaiseTime); // ensure event to be raised

			await api.StopAsync(CancellationToken.None);

			// assert
			Assert.AreEqual(1, requestCallback.Count);
			Assert.AreEqual($"{{\"apikey\":\"{apiKey}\"}}", requestCallback.First());

			Assert.AreEqual(2, receivedEvents.Count);
			Assert.AreEqual(0, receivedFeedback.Count);
			Assert.AreEqual(0, receivedFeedbackSummary.Count);
			Assert.AreEqual(0, receivedNewsSummary.Count);
			Assert.AreEqual(1, receivedStatus.Count);
			Assert.AreEqual(0, receivedStatusSummary.Count);
			Assert.AreEqual(0, receivedVehicleSummary.Count);

			Assert.AreEqual(date, receivedStatus.First().Timestamp?.ToUniversalTime());
			Assert.IsNotNull(receivedStatus.First().Status);

			Assert.AreEqual(payload.Status.Name, receivedStatus.First().Status.Name);
			Assert.AreEqual(payload.Status.Status, receivedStatus.First().Status.Status);
			Assert.AreEqual(payload.Status.Latitude, receivedStatus.First().Status.Latitude);
			Assert.AreEqual(payload.Status.Longitude, receivedStatus.First().Status.Longitude);
			Assert.AreEqual(payload.Status.AdditionalInformation, receivedStatus.First().Status.AdditionalInformation);
		}

		[TestMethod]
		public async Task ShouldReceiveStatusSummary()
		{
			// arrange
			string apiKey = "abc";
			responseQueue.Enqueue(JsonConvert.SerializeObject(authorized));

			payload.Feedback = null;
			payload.FeedbackSummary = null;
			payload.NewsSummary = null;
			payload.Status = null;
			//payload.StatusSummary = null;
			payload.VehicleSummary = null;

			using var server = CreateServer();
			using var api = GetApi(apiKey);

			// act
			await api.StartAsync(CancellationToken.None);
			await Task.Delay(eventRaiseTime); // ensure event to be raised

			await server.SendMessage(JsonConvert.SerializeObject(payload));
			await Task.Delay(eventRaiseTime); // ensure event to be raised

			await api.StopAsync(CancellationToken.None);

			// assert
			Assert.AreEqual(1, requestCallback.Count);
			Assert.AreEqual($"{{\"apikey\":\"{apiKey}\"}}", requestCallback.First());

			Assert.AreEqual(2, receivedEvents.Count);
			Assert.AreEqual(0, receivedFeedback.Count);
			Assert.AreEqual(0, receivedFeedbackSummary.Count);
			Assert.AreEqual(0, receivedNewsSummary.Count);
			Assert.AreEqual(0, receivedStatus.Count);
			Assert.AreEqual(1, receivedStatusSummary.Count);
			Assert.AreEqual(0, receivedVehicleSummary.Count);

			Assert.AreEqual(date, receivedStatusSummary.First().Timestamp?.ToUniversalTime());
			Assert.IsNotNull(receivedStatusSummary.First().Summary);
			Assert.AreEqual(date, receivedStatusSummary.First().Summary.Timestamp.Value.ToUniversalTime());

			for (int i = 0; i < receivedStatusSummary.First().Summary.FunctionGroups.Count; i++)
			{
				Assert.AreEqual(payload.StatusSummary.FunctionGroups.ElementAt(i).Id, receivedStatusSummary.First().Summary.FunctionGroups.ElementAt(i).Id);
				Assert.AreEqual(payload.StatusSummary.FunctionGroups.ElementAt(i).Message, receivedStatusSummary.First().Summary.FunctionGroups.ElementAt(i).Message);
				
				Assert.AreEqual(JsonConvert.SerializeObject(payload.StatusSummary.FunctionGroups.ElementAt(i).GroupCounts), JsonConvert.SerializeObject(receivedStatusSummary.First().Summary.FunctionGroups.ElementAt(i).GroupCounts));
			}
		}

		[TestMethod]
		public async Task ShouldReceiveVehicleSummary()
		{
			// arrange
			string apiKey = "abc";
			responseQueue.Enqueue(JsonConvert.SerializeObject(authorized));

			payload.Feedback = null;
			payload.FeedbackSummary = null;
			payload.NewsSummary = null;
			payload.Status = null;
			payload.StatusSummary = null;
			//payload.VehicleSummary = null;

			using var server = CreateServer();
			using var api = GetApi(apiKey);

			// act
			await api.StartAsync(CancellationToken.None);
			await Task.Delay(eventRaiseTime); // ensure event to be raised

			await server.SendMessage(JsonConvert.SerializeObject(payload));
			await Task.Delay(eventRaiseTime); // ensure event to be raised

			await api.StopAsync(CancellationToken.None);

			// assert
			Assert.AreEqual(1, requestCallback.Count);
			Assert.AreEqual($"{{\"apikey\":\"{apiKey}\"}}", requestCallback.First());

			Assert.AreEqual(2, receivedEvents.Count);
			Assert.AreEqual(0, receivedFeedback.Count);
			Assert.AreEqual(0, receivedFeedbackSummary.Count);
			Assert.AreEqual(0, receivedNewsSummary.Count);
			Assert.AreEqual(0, receivedStatus.Count);
			Assert.AreEqual(0, receivedStatusSummary.Count);
			Assert.AreEqual(1, receivedVehicleSummary.Count);

			Assert.AreEqual(date, receivedVehicleSummary.First().Timestamp?.ToUniversalTime());
			Assert.IsNotNull(receivedVehicleSummary.First().VehicleGroups);

			for (int i = 0; i < receivedVehicleSummary.First().VehicleGroups.Count; i++)
			{
				Assert.AreEqual(payload.VehicleSummary.ElementAt(i).Id, receivedVehicleSummary.First().VehicleGroups.ElementAt(i).Id);
				Assert.AreEqual(payload.VehicleSummary.ElementAt(i).GroupId, receivedVehicleSummary.First().VehicleGroups.ElementAt(i).GroupId);
				Assert.AreEqual(payload.VehicleSummary.ElementAt(i).Name, receivedVehicleSummary.First().VehicleGroups.ElementAt(i).Name);
				
				Assert.AreEqual(JsonConvert.SerializeObject(payload.VehicleSummary.ElementAt(i).Vehicles), JsonConvert.SerializeObject(receivedVehicleSummary.First().VehicleGroups.ElementAt(i).Vehicles));
			}
		}
	}
}
