﻿namespace AMWD.Net.Api.PowerAlarm.WebSocket.Test.Utils
{
	internal class ConnectedClient
	{
		public ConnectedClient(int socketId, System.Net.WebSockets.WebSocket socket)
		{
			SocketId = socketId;
			Socket = socket;
		}

		public int SocketId { get; private set; }

		public System.Net.WebSockets.WebSocket Socket { get; private set; }
	}
}
