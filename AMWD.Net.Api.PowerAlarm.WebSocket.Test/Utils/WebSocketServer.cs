﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Net.WebSockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace AMWD.Net.Api.PowerAlarm.WebSocket.Test.Utils
{
	// Code found:
	// https://github.com/MV10/WebSocketExample/blob/6f2fae728bbf371591268976be6b05fb0a10b14c/WebSocketExample/WebSocketServer.cs
	[System.Diagnostics.CodeAnalysis.ExcludeFromCodeCoverage]
	internal class WebSocketServer : IDisposable
	{
		private readonly HttpListener listener;

		private readonly CancellationTokenSource socketLoopTokenSource;
		private readonly CancellationTokenSource listenerLoopTokenSource;

		private int socketCounter = 0;

		private bool serverIsRunning = true;

		private readonly ConcurrentDictionary<int, ConnectedClient> clients = new();

		public WebSocketServer()
		{
			Port = GetAvailablePort();

			socketLoopTokenSource = new CancellationTokenSource();
			listenerLoopTokenSource = new CancellationTokenSource();
			listener = new HttpListener();
			listener.Prefixes.Add($"http://localhost:{Port}/");
			listener.Start();

			if (listener.IsListening)
			{
				Task.Run(() => ListenerProcessingLoopAsync().ConfigureAwait(false));
			}
			else
			{
				throw new NotSupportedException();
			}
		}

		public int Port { get; }

		public async Task SendMessage(string msg)
		{
			byte[] msgBytes = Encoding.UTF8.GetBytes(msg);

			var list = clients.ToList();
			foreach (var client in list)
			{
				await client.Value.Socket.SendAsync(msgBytes, WebSocketMessageType.Text, endOfMessage: true, CancellationToken.None);
			}
		}

		public Func<string, string> ProcessRequest { get; set; }

		public void Dispose()
		{
			if (listener?.IsListening ?? false && serverIsRunning)
			{
				serverIsRunning = false;
				CloseAllSocketsAsync().Wait();
				listenerLoopTokenSource.Cancel();
				listener.Stop();
				listener.Close();
			}
		}

		private async Task ListenerProcessingLoopAsync()
		{
			var cancellationToken = listenerLoopTokenSource.Token;
			try
			{
				while (!cancellationToken.IsCancellationRequested)
				{
					HttpListenerContext context = await listener.GetContextAsync();
					if (serverIsRunning)
					{
						if (context.Request.IsWebSocketRequest)
						{
							HttpListenerWebSocketContext wsContext = null;
							try
							{
								wsContext = await context.AcceptWebSocketAsync(subProtocol: null);
								int socketId = Interlocked.Increment(ref socketCounter);
								var client = new ConnectedClient(socketId, wsContext.WebSocket);
								clients.TryAdd(socketId, client);
								_ = Task.Run(() => SocketProcessingLoopAsync(client).ConfigureAwait(false));
							}
							catch (Exception)
							{
								context.Response.StatusCode = 500;
								context.Response.StatusDescription = "WebSocket upgrade failed";
								context.Response.Close();
								return;
							}
						}
						else
						{
							context.Response.StatusCode = 400;
							context.Response.StatusDescription = "Bad Request";
							context.Response.Close();
						}
					}
					else
					{
						context.Response.StatusCode = 409;
						context.Response.StatusDescription = "Server is shutting down";
						context.Response.Close();
						return;
					}
				}
			}
			catch
			{ /* keep things quiet - it's just an UnitTest */ }
		}

		private async Task SocketProcessingLoopAsync(ConnectedClient client)
		{
			var socket = client.Socket;
			var loopToken = socketLoopTokenSource.Token;
			try
			{
				var buffer = System.Net.WebSockets.WebSocket.CreateServerBuffer(4096);
				while (socket.State != WebSocketState.Closed && socket.State != WebSocketState.Aborted && !loopToken.IsCancellationRequested)
				{
					var receiveResult = await client.Socket.ReceiveAsync(buffer, loopToken);
					if (!loopToken.IsCancellationRequested)
					{
						if (client.Socket.State == WebSocketState.CloseReceived && receiveResult.MessageType == WebSocketMessageType.Close)
						{
							await socket.CloseOutputAsync(WebSocketCloseStatus.NormalClosure, "Acknowledge Close frame", CancellationToken.None);
						}

						if (client.Socket.State == WebSocketState.Open)
						{
							switch (receiveResult.MessageType)
							{
								case WebSocketMessageType.Text:
									string text = Encoding.UTF8.GetString(new ArraySegment<byte>(buffer.Array, 0, receiveResult.Count));
									string textResponse = ProcessRequest(text);

									if (!string.IsNullOrWhiteSpace(textResponse))
										await socket.SendAsync(Encoding.UTF8.GetBytes(textResponse), receiveResult.MessageType, receiveResult.EndOfMessage, CancellationToken.None);

									break;
								default:
									await socket.SendAsync(new ArraySegment<byte>(buffer.Array, 0, receiveResult.Count), receiveResult.MessageType, receiveResult.EndOfMessage, CancellationToken.None);
									break;
							}
						}
					}
				}
			}
			catch (OperationCanceledException)
			{
				// normal upon task/token cancellation, disregard
			}
			catch (Exception)
			{ /* keep things quiet - it's just an UnitTest */ }
			finally
			{
				if (client.Socket.State != WebSocketState.Closed)
					client.Socket.Abort();

				if (clients.TryRemove(client.SocketId, out _))
					socket.Dispose();
			}
		}

		private async Task CloseAllSocketsAsync()
		{
			var disposeQueue = new List<System.Net.WebSockets.WebSocket>(clients.Count);
			while (!clients.IsEmpty)
			{
				var client = clients.ElementAt(0).Value;
				Console.WriteLine($"Closing Socket {client.SocketId}");

				Console.WriteLine("... ending broadcast loop");

				if (client.Socket.State == WebSocketState.Open)
				{
					var timeout = new CancellationTokenSource(2500);
					try
					{
						await client.Socket.CloseOutputAsync(WebSocketCloseStatus.NormalClosure, "Closing", timeout.Token);
					}
					catch
					{ /* keep things quiet - it's just an UnitTest */ }
				}

				if (clients.TryRemove(client.SocketId, out _))
				{
					disposeQueue.Add(client.Socket);
				}
			}

			socketLoopTokenSource.Cancel();
			foreach (var socket in disposeQueue)
				socket.Dispose();
		}

		private static int GetAvailablePort()
		{
			var listener = new TcpListener(IPAddress.Loopback, 0);
			listener.Start();
			int port = (listener.LocalEndpoint as IPEndPoint).Port;
			listener.Stop();
			return port;
		}
	}
}
