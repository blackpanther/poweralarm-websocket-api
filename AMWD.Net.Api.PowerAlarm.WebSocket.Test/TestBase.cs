using System;
using System.Collections.Generic;
using AMWD.Net.Api.PowerAlarm.WebSocket.EventArguments;
using AMWD.Net.Api.PowerAlarm.WebSocket.Models;
using AMWD.Net.Api.PowerAlarm.WebSocket.Test.Utils;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AMWD.Net.Api.PowerAlarm.WebSocket.Test
{
	[TestClass]
	[System.Diagnostics.CodeAnalysis.ExcludeFromCodeCoverage]
	public class TestBase
	{
		private int serverPort = 0;

		internal readonly DateTime date = new DateTime(2022, 06, 15, 13, 14, 15, DateTimeKind.Utc);

		internal List<string> requestCallback;
		internal Queue<string> responseQueue;

		internal List<PowerAlarmEventArgs> receivedEvents;
		internal List<StatusSummaryEventArgs> receivedStatusSummary;
		internal List<StatusEventArgs> receivedStatus;
		internal List<NewsSummaryEventArgs> receivedNewsSummary;
		internal List<VehicleSummaryEventArgs> receivedVehicleSummary;
		internal List<FeedbackSummaryEventArgs> receivedFeedbackSummary;
		internal List<FeedbackEventArgs> receivedFeedback;

		internal PowerAlarmPayload authorized;
		internal PowerAlarmPayload payload;

		[TestInitialize]
		public void InitializeTest()
		{
			requestCallback = new List<string>();
			responseQueue = new Queue<string>();

			receivedEvents = new List<PowerAlarmEventArgs>();
			receivedStatusSummary = new List<StatusSummaryEventArgs>();
			receivedStatus = new List<StatusEventArgs>();
			receivedNewsSummary = new List<NewsSummaryEventArgs>();
			receivedVehicleSummary = new List<VehicleSummaryEventArgs>();
			receivedFeedbackSummary = new List<FeedbackSummaryEventArgs>();
			receivedFeedback = new List<FeedbackEventArgs>();

			authorized = new PowerAlarmPayload
			{
				Timestamp = date,
				IsAccepted = true,
				IsWebSocket = true
			};

			payload = new PowerAlarmPayload
			{
				Timestamp = date,
				IsWebSocket = true,
				Feedback = new Feedback
				{
					Timestamp = date,
					Id = 1234,
					Message = "Some Feedback",
					Details = new List<FeedbackDetail>
					{
						new FeedbackDetail
						{
							Color = "#123123",
							Description = "Feedback Detail 1",
							Contacts = new List<FeedbackUser>
							{
								new FeedbackUser
								{
									Name = "Bob Stone",
									Status = "1"
								}
								
							}
						},
						new FeedbackDetail
						{
							Color = "#321312",
							Description = "Feedback Detail 2",
							Contacts = new List<FeedbackUser>
							{
								new FeedbackUser
								{
									Name = "Calvin Joyner",
									Status = "2",
									AdditionalInformation = "Your Ad Here"
								},
								new FeedbackUser
								{
									Name = "Maggie Joyner",
									Status = "1",
									AdditionalInformation = "The Beauty"
								}

							}
						}
					}
				},
				FeedbackSummary = new List<FeedbackSummary>
				{
					new FeedbackSummary
					{
						Id = 9876,
						Timestamp = date,
						Group = "abc",
						Message = "Foo Bar!",
						FunctionGroups = new List<FunctionGroup>
						{
							new FunctionGroup
							{
								Id = 4711,
								Message = "Ready to go",
								GroupCounts = new List<FunctionGroupCount>
								{
									new FunctionGroupCount
									{
										Status = 1,
										Count = 5,
										Text = "Ready to go",
										Color = "#00ff00"
									}
								}
							}
						}
					}
				},
				NewsSummary = new List<News>
				{
					new News
					{
						Id = 123,
						StartTimestamp = date,
						EndTimestamp = date.AddHours(1),
						Message = "++++ EXTRA NEWS ++++"
					}
				},
				Status = new UserStatus
				{
					Name = "Robbi Weirdicht",
					Status = 6,
					Latitude = 1.23f,
					Longitude = 4.56f,
					AdditionalInformation = "Not seen for a long time"
				},
				StatusSummary = new StatusSummary
				{
					Timestamp = date,
					FunctionGroups = new List<FunctionGroup>
					{
						new FunctionGroup
						{
							Id = 4711,
							Message = "Ready to go",
							GroupCounts = new List<FunctionGroupCount>
							{
								new FunctionGroupCount
								{
									Status = 1,
									Count = 5,
									Text = "Ready to go",
									Color = "#00ff00"
								}
							}
						}
					}
				},
				VehicleSummary = new List<VehicleGroup>
				{
					new VehicleGroup
					{
						Id = 321,
						GroupId = 42,
						Name = "BigCars",
						Vehicles = new List<Vehicle>
						{
							new Vehicle
							{
								Id = 21,
								GroupId = 42,
								Name = "Moggi",
								Timestamp = date,
								Status = 6,
								Color = "#0099ff",
								Fms = "A543B21C98",
								Opta = "FD83ED218A"
							}
						}
					}
				}
			};
		}

		internal PowerAlarmWebSocketApi GetApi(string apiKey)
		{
			var api = new PowerAlarmWebSocketApi($"ws://localhost:{serverPort}")
			{
				ApiKey = apiKey
			};

			api.Received += (_, args) => receivedEvents.Add(args);
			api.ReceivedStatusSummary += (_, args) => receivedStatusSummary.Add(args);
			api.ReceivedStatus += (_, args) => receivedStatus.Add(args);
			api.ReceivedNewsSummary += (_, args) => receivedNewsSummary.Add(args);
			api.ReceivedVehicleSummary += (_, args) => receivedVehicleSummary.Add(args);
			api.ReceivedFeedbackSummary += (_, args) => receivedFeedbackSummary.Add(args);
			api.ReceivedFeedback += (_, args) => receivedFeedback.Add(args);

			return api;
		}

		internal WebSocketServer CreateServer()
		{
			var server = new WebSocketServer
			{
				ProcessRequest = (request) =>
				{
					requestCallback.Add(request);
					string response = responseQueue.Dequeue();
					return response;
				}
			};

			serverPort = server.Port;

			return server;
		}
	}
}
