﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AMWD.Net.Api.PowerAlarm.WebSocket.Exceptions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;

namespace AMWD.Net.Api.PowerAlarm.WebSocket.Test
{
	[TestClass]
	[System.Diagnostics.CodeAnalysis.ExcludeFromCodeCoverage]
	public class ConstructorTests : TestBase
	{
		[TestMethod]
		public void ShouldCreateDefault()
		{
			// arrange

			// act
			using var api = new PowerAlarmWebSocketApi();

			// assert
			Assert.IsNotNull(api);
		}

		[TestMethod]
		public void ShouldCreateInsecure()
		{
			// arrange

			// act
			using var api = new PowerAlarmWebSocketApi(useInsecureConnection: true);

			// assert
			Assert.IsNotNull(api);
		}

		[TestMethod]
		[ExpectedException(typeof(ObjectDisposedException))]
		public async Task ShouldThrowExceptionOnDisposed()
		{
			// arrange
			var api = new PowerAlarmWebSocketApi();

			// act
			api.Dispose();
			await api.StartAsync(CancellationToken.None);

			// assert - Exception
		}

		[TestMethod]
		public void ShouldAllowMultipleDisposes()
		{
			// arrange
			var api = new PowerAlarmWebSocketApi();

			// act
			api.Dispose();
			api.Dispose();

			// assert
			Assert.IsNotNull(api);
		}

		[TestMethod]
		[ExpectedException(typeof(ApiKeyException))]
		public async Task ShouldThrowExceptionOnMissingApikey()
		{
			// arrange
			using var api = new PowerAlarmWebSocketApi();

			// act
			await api.StartAsync(CancellationToken.None);

			// assert - Exception
		}

		[TestMethod]
		public async Task ShouldConnectAndAuthenticate()
		{
			// arrange
			string apiKey = "abc";
			responseQueue.Enqueue(JsonConvert.SerializeObject(authorized));

			using var server = CreateServer();
			using var api = GetApi(apiKey);

			// act
			await api.StartAsync(CancellationToken.None);
			await Task.Delay(100); // ensure event to be raised
			await api.StopAsync(CancellationToken.None);

			// assert
			Assert.AreEqual(1, requestCallback.Count);
			Assert.AreEqual(1, receivedEvents.Count);
			Assert.AreEqual($"{{\"apikey\":\"{apiKey}\"}}", requestCallback.First());
		}
	}
}
