# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased](https://git.am-wd.de/andreasmueller/poweralarm-websocket-api/compare/v1.0.0...main) - 0000-00-00
_nothing changed yet_


## [v1.0.0](https://git.am-wd.de/andreasmueller/poweralarm-websocket-api/compare/v0.1.0...v1.0.0) - 0000-00-00
### Added
- UnitTests

### Fixed
- Events are implemented correctly


## [v0.1.0](https://git.am-wd.de/andreasmueller/poweralarm-websocket-api/commits/v0.1.0) - 2022-02-12

Initial release - UnitTests missing due to a non-mockable sealed class.
